package com.scud.gasvas;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Garnau on 04/04/2018.
 */

public class MainMenu extends AppCompatActivity {

    private Button btnVideo, btnInfo, btnRec;
    String username, lastname;
    Bitmap userPhoto;
    ImageView photoTaken;
    TextView nameView, lastNameView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        InitializeUI();

        //Inicialitzem variables i OnClick
        btnVideo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //setContentView(R.layout.playvideo_layout);

                MainMenu.this.startActivity(new Intent(MainMenu.this, PlayVideo.class));
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //setContentView(R.layout.activity_menu);

                MainMenu.this.startActivity(new Intent(MainMenu.this, Info.class));
            }
        });

        btnRec.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //setContentView(R.layout.recordvideo_layout);

                MainMenu.this.startActivity(new Intent(MainMenu.this, RecordVideo.class));
            }
        });

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            username = extras.getString("name");
            lastname = extras.getString("lastname");

            userPhoto = extras.getParcelable("photo");
            if (extras.getParcelable("photo") == null){
                userPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.buu);
            }
        }

        nameView.setText(username);
        lastNameView.setText(lastname);
        photoTaken.setImageBitmap(userPhoto);

    }

    private void InitializeUI() {

        photoTaken = findViewById(R.id.photoTaken);
        nameView = findViewById(R.id.name);
        lastNameView = findViewById(R.id.lastname);
        btnVideo = findViewById(R.id.btnVideo);
        btnInfo = findViewById(R.id.btnInfo);
        btnRec = findViewById(R.id.btnRec);

        btnVideo.setTextColor(getApplication().getResources().getColor(R.color.blanc));
        btnInfo.setTextColor(getApplication().getResources().getColor(R.color.blanc));
        btnRec.setTextColor(getApplication().getResources().getColor(R.color.blanc));

    }

    public void onClick(View view) {

    }

}
