package com.scud.gasvas;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    static final int CAMERA_APP_CODE = 101;

    ImageView photoTaken;
    Button photoBtn, confirmBtn;
    EditText nameET, lastNameET;
    String nameETContent, lastNameETContent;
    Intent goToMenuIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeUI();

        photoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_APP_CODE);

                }
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameETContent = nameET.getText().toString();
                lastNameETContent = lastNameET.getText().toString();

                goToMenuIntent.putExtra("name", nameETContent);
                goToMenuIntent.putExtra("lastname", lastNameETContent);
                MainActivity.this.startActivity(goToMenuIntent);
            }
        });

    }

    private void InitializeUI() {

        photoTaken = findViewById(R.id.photoTaken);
        photoBtn = findViewById(R.id.photoBtn);
        confirmBtn = findViewById(R.id.confirmBtn);
        nameET = findViewById(R.id.name);
        lastNameET = findViewById(R.id.lastname);

        confirmBtn.setTextColor(getApplication().getResources().getColor(R.color.blanc));
        photoBtn.setTextColor(getApplication().getResources().getColor(R.color.blanc));

        goToMenuIntent = new Intent(MainActivity.this, MainMenu.class);
        goToMenuIntent.putExtra("photo", R.drawable.buu);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_APP_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            photoTaken.setImageBitmap(imageBitmap);
            goToMenuIntent.putExtra("photo", imageBitmap);
        }

    }


}
