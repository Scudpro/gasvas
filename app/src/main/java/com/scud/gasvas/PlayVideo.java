package com.scud.gasvas;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by Garnau on 09/04/2018.
 */

public class PlayVideo extends AppCompatActivity implements View.OnClickListener {

    private MediaController mediaController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mediaController = new MediaController(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playvideo_layout);

        ObrirGif();

    }

    private void ObrirGif() {

        VideoView videoView = (VideoView) findViewById(R.id.videoView);

        //Uri video = Uri.parse("http://35.177.198.220/gokuvsvegeta.mp4");

        //videoView.setVideoURI(video);
        videoView.setMediaController(mediaController);
        videoView.setVideoPath("http://35.177.198.220/gokuvsvegeta.mp4");


        videoView.start();

    }

    @Override
    public void onClick(View view) {

    }

}
