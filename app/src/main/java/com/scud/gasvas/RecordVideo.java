package com.scud.gasvas;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.pm.PackageManager;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

enum Estat {
    GRAVANT, ATURAT
};

public class RecordVideo extends Activity implements OnClickListener {

    private Button btnGravar;
    private MediaRecorder gravador;
    private Camera camera;
    private PrevisualitzacioCamera prev;
    private Estat estatGravacio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recordvideo_layout);

        btnGravar = (Button) findViewById(R.id.btnGravar);
        btnGravar.setOnClickListener(this);

        if (teCamera()) {
            camera = getCamera();
            setCameraDisplayOrientation(this, Camera.CameraInfo.CAMERA_FACING_FRONT, camera);
            if (camera != null) {
                prev = new PrevisualitzacioCamera(this, camera);
                FrameLayout frameLayout = (FrameLayout) findViewById(R.id.camera_preview);
                frameLayout.addView(prev);
            }
        } else {
            Dialeg.mostrarDialeg(this, getApplicationInfo().name,
                    "El dispositiu no té càmera");
        }

        estatGravacio = Estat.ATURAT;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.gravacio_video, menu);
        return true;
    }

    /*
     * Comprova si el dispositiu té càmera
     */
    private boolean teCamera() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Obtenir i obrir l'objecte Camera
     */
    public Camera getCamera() {
        boolean found = false;
        int i;
//        for (i=0; i< Camera.getNumberOfCameras(); i++) {
//            Camera.CameraInfo newInfo = new Camera.CameraInfo();
//            Camera.getCameraInfo(i, newInfo);
//            //Utilitzar càmera frontal.
//            if (newInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                found = true;
//                break;
//            }
//        }
        Camera c = Camera.open(1);
        return c;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnGravar) {
            if (estatGravacio == Estat.ATURAT) {
                prepararGravacio();
                gravador.start();
                btnGravar.setBackgroundColor(getResources().getColor(
                        R.color.blau));
                btnGravar.setText(R.string.btnAturarGravacio);
                estatGravacio = Estat.GRAVANT;
            } else {
                aturarGravacio();
                btnGravar.setBackgroundColor(getResources().getColor(
                        R.color.taronja));
                btnGravar.setText(R.string.btnIniciarGravacio);
                estatGravacio = Estat.ATURAT;
            }
        }
    }

    /*
     * Obtenir l'orientació de la càmera en funció de l'orientació de la pantalla
     */
    private static int getCameraOrientation(Activity activity, int idCamera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(idCamera, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        //Part de previsualització.
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensar l'efecte mirall
        } else { // càmera de darrera
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    /*
     * Assignar l'orientació de la càmera (previsualització)
     */
    public static void setCameraDisplayOrientation(Activity activity, int idCamera, android.hardware.Camera camera) {
        camera.setDisplayOrientation(getCameraOrientation(activity, Camera.CameraInfo.CAMERA_FACING_FRONT));
    }

    /*
     * Configurar els paràmetres del gravador
     */
    private void prepararGravacio() {
        if (gravador != null) {
            aturarGravacio();
        }
        try {
            camera.unlock();
            gravador = new MediaRecorder();
            // quina càmera utilitza el gravador
            gravador.setCamera(camera);
            // orígen del vídeo i de l'àudio
            gravador.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            gravador.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            // formats de sortida
            gravador.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            // codificació del vídeo i de l'àudio
            gravador.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            gravador.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            // establir el nom del fitxer de sortida
            gravador.setOutputFile(getNomFitxer().getAbsolutePath());
            // assignar la previsualització
            gravador.setPreviewDisplay(prev.getHolder().getSurface());
            // indicar amb quina orientació s'ha de gravar
            gravador.setOrientationHint(270);//getCameraOrientation(this, Camera.CameraInfo.CAMERA_FACING_FRONT));


            // tot preparat!
            gravador.prepare();
        } catch (Exception e) {
            Dialeg.mostrarDialeg(this, "Gravació",
                    "ERROR en iniciar la gravació: " + e.getMessage());
            aturarGravacio();
        }
    }

    /*
     * Establir el nom del fitxer on es desarà el vídeo
     */
    private File getNomFitxer() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "video_" + timeStamp + ".3gpp";
        File path = new File(Environment.getExternalStorageDirectory(),
                this.getPackageName());
        if (!path.exists())
            path.mkdirs();

        return new File(path, imageFileName);
    }

    private void aturarGravacio() {
        if (gravador != null) {
            gravador.reset(); // esborrar la configuració del gravador
            gravador.release(); // alliberar els recursos de l'objecte
            gravador = null;
            camera.lock(); // bloquejar la camera
        }
    }

    private void alliberarCamera() {
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        aturarGravacio();
        alliberarCamera();
    }
}