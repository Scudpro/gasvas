package com.scud.gasvas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by sergi on 09-Apr-18.
 */

public class Info extends AppCompatActivity{

    TextView goku, vegeta, gohan, cor_petit, tenshinhan, krillin, trunks;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        initializeUI();

    }

    private void initializeUI() {

        goku = findViewById(R.id.goku);
        vegeta = findViewById(R.id.vegeta);
        gohan = findViewById(R.id.gohan);
        cor_petit = findViewById(R.id.cor_petit);
        tenshinhan = findViewById(R.id.tenshinhan);
        krillin = findViewById(R.id.krillin);
        trunks = findViewById(R.id.trunks);

    }
}
