package com.scud.gasvas;

/**
 * Created by Scud on 03/04/2018.
 */
import android.app.AlertDialog;
import android.content.Context;

public class Dialeg {

    public static void mostrarDialeg(Context context, String titol, String missatge) {
        AlertDialog.Builder dialeg = new AlertDialog.Builder(context);
        dialeg.create();
        dialeg.setTitle(titol);
        dialeg.setMessage(missatge);
        dialeg.show();
    }
}